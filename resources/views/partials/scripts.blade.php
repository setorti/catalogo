{!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.0/jquery.min.js') !!}
{!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js') !!}
{!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/js/bootstrap.min.js') !!}
{!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/vue/2.5.16/vue.min.js') !!}
{!! Html::script('//cdnjs.cloudflare.com/ajax/libs/jquery-scrollTo/2.1.2/jquery.scrollTo.min.js') !!}
{!! Html::script('https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js') !!}
{!! Html::script('https://unpkg.com/vue-chartjs/dist/vue-chartjs.min.js') !!}

{!! Html::script(URL::asset('/js/app.js')) !!}
{!! Html::script(URL::asset('/js/tinymce.min.js')) !!}

{!! Html::style('https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.min.css') !!}
{!! Html::style('https://use.fontawesome.com/releases/v5.0.13/css/all.css') !!}
{!! Html::style(URL::asset('/css/style.css')) !!}