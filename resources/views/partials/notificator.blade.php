@if(\Session::has('message') && \Session::has('type'))
	<div class="alert alert-{{ \Session::get('type') }} alert-dismissible fade show" role="alert">
		@if(is_array(\Session::get('message')))
			<ul class="notifications-list">
			@foreach( \Session::get('message') as $message )				
				<li class="message">{{ $message }}</li>
			@endforeach
			</ul>
		@else
			<div class="message">{{ \Session::get('message')  }}</div>	
		@endif		
	</div>
@endif