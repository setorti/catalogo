<ul class="form-inline my-2 my-lg-0 menu">
	@if(Auth::check())

	<li class="nav-item dropdown">
		<a href="javascript:void(0)" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
			<i class="fa fa-user"></i> {{ Auth::user()->name }}
			<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
				<a class="dropdown-item" href="{{ url('users/edit', Auth::user()['id']) }}"><i class="fa fa-user"></i> Meu Perfil</a>
				@if(Auth::user()['admin'])
				<a class="dropdown-item" href="{{ url('/admin') }}"><i class="fas fa-columns"></i> Painel de Administrador</a>
				@endif
				<a class="dropdown-item" href="{{ URL::to('/logout') }}"><i class="fa fa-sign-out"></i> Sair</a>
			</div>
		</a>
	</li>

	@else

	<li class="nav-item item">
		<a class="nav-link" href="{{ URL::to('/login') }}"><i class="fa fa-sign-in" aria-hidden="true"></i> Entrar</a>
	</li>
	<li class="nav-item item">
		<a class="nav-link" href="{{ URL::to('/register') }}"><i class="fa fa-user-plus" aria-hidden="true"></i> Cadastrar</a>
	</li>

	@endif
</ul>