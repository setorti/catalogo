@if($number_of_pages > 0)   
    <ul class="pagination justify-content-center">
        @if($current_page > 1)
            <li class="page">
                <a class="page-link" 
                href="{{ URL::to('/search', ['page' => 1, 'search' => $search]) }}"><span aria-hidden="true">&laquo;</span></a>
            </li>
        @endif
        @if( $current_page - 2 > 0 )
            <li class="page">
                <a class="page-link" 
                href="{{ URL::to('/search', ['page' => $current_page - 2, 'search' => $search]) }}">{{ $current_page - 2 }}</a>
            </li>
        @endif
        @if( $current_page - 1 > 0 )
            <li class="page">
                <a class="page-link" 
                href="{{ URL::to('/search', ['page' => $current_page - 1, 'search' => $search]) }}">{{ $current_page - 1 }}</a>
            </li>
        @endif      
        <li class="page"> <!-- current page -->
            <a class="page-link current-page" 
            href="{{ URL::to('/search', ['page' => $current_page, 'search' => $search]) }}">{{ $current_page }}</a>
        </li>       
        @if( $current_page + 1 <= $number_of_pages )
            <li class="page">
                <a class="page-link" 
                href="{{ URL::to('/search', ['page' => $current_page + 1, 'search' => $search]) }}">{{ $current_page + 1 }}</a>
            </li>
        @endif
        @if( $current_page + 2 <= $number_of_pages )
            <li class="page">
                <a class="page-link" 
                href="{{ URL::to('/search', ['page' => $current_page + 2, 'search' => $search]) }}">{{ $current_page + 2 }}</a>
            </li>
        @endif
        @if($current_page < $number_of_pages)
            <li class="page">
                <a class="page-link" 
                href="{{ URL::to('/search', ['page' => $number_of_pages, 'search' => $search]) }}"><span aria-hidden="true">&raquo;</span></a>
            </li>
        @endif
    </ul>
    @endif