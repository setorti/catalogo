@extends('main')
@section("content")
<div class="row">
    <div class="col-6">
        <section class="welcome-title">
            <h1>SEJA<br> BEM VINDO,<br> PODE ENTRAR!</h1>
        </section>
        <section class="welcome-text">
            <p>
                
                Esse é o ambiente da Criação Shalom.
                Vamos aprender juntos a traduzir o Carisma Shalom visualmente e assim viver bem e cada vez melhor a nossa missão dentro do ministério de comunicação e da Comunidade.
                Se não sabe por onde começar, clica na aba workshops lá em cima, comece a aprender e divirta-se!
                É uma alegria ter você por aqui e servir com você.
        
            </p>
            <p>Deus abençoe.</p>
           <p>Shalom!</p> 
        </section>
    </div>
    <div class="col-6 welcome-picture">
        <picture>
            <img src="{{ URL::asset('img/monitorSH.png') }}" alt="">
        </picture>
    </div>
</div>
@endsection