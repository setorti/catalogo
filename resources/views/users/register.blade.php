@extends('main')
@section("content")
<section>
	<div class="card standard-form register-form row">
		<div class="col-sm-7">
			<div class="card-header register-title">
				<h3>cadastrar-se</h3>
			</div>

			<div class="card-body register-body">
				{!! Form::open(['url' => '/auth/register', 'method' => 'post']) !!}
				<div class="form-group">
					<label for="name">Nome</label>
					<input class="form-control" name="name">
				</div>
				<div class="form-group">
					<label for="email">Email</label>
					<input class="form-control" name="email">
				</div>
				<div class="form-group">
					<label for="password">Senha</label>
					<input type="password" class="form-control" name="password">
					<div class="register-email-warning">Somente emails do domínio <b>@comshalom.org</b> <br> poderão ser cadastrados.</div>
				</div>
				<div class="form-group register-buttons">
					<button class="btn register-btn">ok</button>
					<button class="btn register-btn-cancel">cancelar</button>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
</section>
@endsection