@extends('main')
@section("content")
<h3>Recuperação de senha</h3>
<h5>Email: <b>{{ $recover_hash->user->email }}</b></h5>
<hr>
{!! Form::open(['url' => "/api/recover/password", 'method' => 'post']) !!}
<input type="hidden" name="recover_hash" value="{{ $recover_hash->hash }}"/>
<div class="form-group">
    <label for="password">Senha</label>
    <input type="password" class="form-control" name="password">
</div>
<div class="form-group">
    <button class="btn btn-black">Alterar Senha</button>
</div>
{!! Form::close() !!}
@endsection