
@extends('main')
@section("content")
<script type="text/javascript">
	(function($) {
		$(document).ready(function() {
			Vue.component('index', {
				template: `
				<div class="campanhas row">
                    <section class="companhas-section mt-5 col-6">
                        <h1>Campanhas</h1>
                        <h1>Liberadas</h1>
                        <div class="campanhas-list mt-5">
                            <ul v-for="file in $parent.files.data">
                                <li>
                                <span>@{{ file.title }}</span>
                                <a v-bind:href="'{{ url('/files/download') }}' + '/' + file.id">link para downloand</a>
                                </li>
                            </ul>
                        </div>
                    </section>
                    <section class="campanhas-search">
                        <div class="campanhas-search-input campanhas-search-title">
                            <label for="search">buscar</label>
                            <input type="text" class="form-control term-search" v-model="$parent.search" v-on:keyup="$parent.searchFieldHandler">
                        </div>
                    </section>
                </div>
				`,
				data: function() {
					return {

					}
				},
			});
			var app = new Vue({
				el: "#app",
				data: {
					currentView: 'index',
					files: { data: [] },
					params: {
						limit: {{ $limit }},
						page: 1,
					},
					search: '',
					tag: 0,
					tags: [],
				},
				watch: {
					tag: function() {
						this.params.page = 1;
						this.fetchFiles();
					},
				},
				methods: {
					fetchFiles: function() {
						$.get("{{ URL::to('/api/files') }}", { page: this.params.page, limit: this.limit, tag: this.tag, s: this.search }, files => {
							let arr = files.data.filter(x=> {
								if(x.file_tags.length === 1 ) {
									if(x.file_tags[0].tag.name === 'Campanhas') {
										return x
									}
								}
							})
							files.data = arr
                            console.log(files)
							this.files = files;
							this.params.page = this.files.current_page;
						});
					},
					searchFieldHandler: function() {
						this.files = [];
						this.params.page = 1;
						this.fetchFiles();
					},
				},
				mounted: function() {
					this.fetchFiles();
				},
			});
		})
	})(jQuery)
</script>
<div id="app">
	<component :is="currentView"></component>
</div>
@endsection