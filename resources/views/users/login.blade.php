@extends('main')
@section("content")
<script type="text/javascript">
	(function($) {
		$(document).ready(function() {
			$("#recover-password").on('click', function(e) {
				e.preventDefault();
				var answer = prompt("Insira seu email para recuperar a senha:", "");
				if( answer != null && answer.length )
				{
					$.ajax({
						type: 'POST',
						data: {email: answer},
						url: "{{ URL::to('/api/recover/request') }}",
						success: function(response) {
							if(response) {
								alert("Recuperação solicitada com sucesso! Verifique seu email.");
								return;
							}

							alert("Falha ao enviar email de recuperação.");
						}
					});
				}
			});
		});
	})(jQuery)
</script>
<section class="row">
	<div class="col-sm-7">
		<div class="card standard-form login-form">
			<div class="card-header">
				<h1>bem vindo,</h1>
				<h1>pode entrar!</h1>
				<h4>acesse sua conta</h3>
			</div>
			<div class="card-body">
				{!! Form::open(['url' => '/auth/login', 'method' => 'post']) !!}
				<div class="form-group w-75">
					<label for="email">login</label>
					<input class="form-control" name="email">
				</div>
				<div class="form-group w-50">
					<label for="password">senha</label>
					<input type="password" class="form-control" name="password">
				</div>
				<div class="form-group">
					<button class="btn btn-entrar">entrar</button>
				</div>
				{!! Form::close() !!}
				<div class="pass">
				<span>para recuperar a senha, </span><a id="recover-password" href="javascript:void(0)"><span>clique aqui.</span></a>
				</div>
				<div class="cadastro">
					<span>para cadastrar-se, </span><a href="{{ URL::to('/register')}}"><span>clique aqui.</span></a>
				</div>
					<!-- <p><a id="recover-password" href="javascript:void(0)"><span>para recuperar a senha,</span> <span>clique aqui</span></a></p>
					<p><a id="cadastro" href="javascript:void(0)"><span>para cadastrar-se,</span> <span>clique aqui</span></a></p> -->
			</div>
		</div>
	</div>
</section class="row">
@endsection