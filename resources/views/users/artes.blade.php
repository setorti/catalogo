@extends('main')
@section("content")
<script type="text/javascript">
	(function($) {
		$(document).ready(function() {
			Vue.component('index', {
				template: `
				<div class="index">
				<nav class="navbar navbar-expand-lg" id="secondary"> <!-- Secondary Navbar -->
						<div class="form-group">
							<h1>artes para</h1>
							<h1>download</h1>
							<div class="institucional-text">
							</div>
						</div>
						<div class="form-group search">
							<label for="search">buscar</label>
							<input type="text" class="form-control term-search" v-model="$parent.search" v-on:keyup="$parent.searchFieldHandler">
						</div>
					</nav>
					<div class="files-grid">
						<h2 v-if="!$parent.files.data.length">Sem Arquivos.</h2>
						<div v-for="file in $parent.files.data" class="file">
							<a href="#" data-toggle="modal" v-bind:data-target="'#file-cover-' + file.id">									
								<img class="thumbnail" v-cloak v-bind:src="'{{ URL::asset('storage/cover') }}' + '/' + file.cover">
							</a>
							<div class="file-content">
							<!--<p class="file-title">@{{ file.title }}</p> 
								<p>
									<a tabindex="0" class="file-description-popover" data-toggle="popover" data-trigger="focus" title="Conteúdo do Arquivo" v-bind:data-content="file.description">Visualizar Conteúdo <i class="fa fa-caret-down"></i></a>
								</p>
								<p class="file-tags">
									<a tabindex="0" class="file-tags-popover" data-toggle="popover" data-trigger="focus" title="Lista de Tags" v-bind:data-content="file.tagString" data-html="true"><i class="fa fa-tags"></i> Tags</a>
								</p> -->
								@if(Auth::check())
								<p class="file-download">
									<a v-bind:href="'{{ url('/files/download') }}' + '/' + file.id" target="_blank">
										<button class="btn btn-black" v-bind:style="{'font-size': '12px'}">
											<i class="fa fa-download"></i>&nbsp;@{{ file.downloads }}
										</button>
									</a>
								</p>
								@endif
							</div>
							<div class="modal fade" tabindex="-1" role="dialog" v-bind:id="'file-cover-' + file.id">
								<div class="modal-dialog" role="document">
									<div class="modal-content">
										<div class="modal-header">
											<h5 class="modal-title">@{{ file.title }}</h5>
											<button type="button" class="close" data-dismiss="modal" aria-label="Close">
												<span aria-hidden="true">&times;</span>
											</button>
										</div>
										<div class="modal-body">
											<img v-bind:src="'{{ URL::asset('storage/cover') }}' + '/' + file.cover">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<ul class="pagination justify-content-center" v-if="$parent.files.data.length > 0">
    					<li v-for="page in $parent.files.last_page" class="page">
    						<a class="page-link" v-bind:class="{ 'current-page' : page == $parent.params.page }" v-on:click="$parent.pageClickHandler(page)">@{{ page }}</a>
    					</li>
					</ul>
				</div>
				`,
				data: function() {
					return {

					}
				},
			});
			var app = new Vue({
				el: "#app",
				data: {
					currentView: 'index',
					files: { data: [] },
					params: {
						limit: {{ $limit }},
						page: 1,
					},
					search: '',
					tag: 0,
					tags: [],
				},
				watch: {
					tag: function() {
						this.params.page = 1;
						this.fetchFiles();
					},
					files: function() {
						
					}
				},
				methods: {
					fetchTags: function() {
						$.get("{{ URL::to('/api/tags') }}", tags => this.tags = tags);
					},
					fetchFiles: function() {
						$.get("{{ URL::to('/api/files') }}", { page: this.params.page, limit: this.limit, tag: this.tag, s: this.search }, files => {
							let arr = files.data.filter(x=> {
								if(x.file_tags.length === 1 ) {
									if(x.file_tags[0].tag.name === 'Artes') {
										return x
									}
								}
							})
							files.data = arr
							this.files = files;
							this.params.page = this.files.current_page;
							this.setupTagString();
						});
					},
					pageClickHandler: function(page) {
						if( page != this.params.page ) {
							this.params.page = page;
							this.fetchFiles();
							$("body").scrollTo("#app", 800, {offset: -100});
						}
					},
					searchFieldHandler: function() {
						this.files = [];
						this.params.page = 1;
						this.fetchFiles();
					},
					setupTagString: function() {
						this.files.data.map(file => {
							if(file.tagString === undefined) file.tagString = '';
							file.file_tags.forEach(file_tag => {
								file.tagString += `- ${file_tag.tag.name}<br>`;
							});
						});

						setTimeout(() => {
							$('.file-tags-popover, .file-description-popover').popover({
  								trigger: 'focus'
							});
						}, 500);
					},
				},
				mounted: function() {
					this.fetchTags();
					this.fetchFiles();
				},
			});
		})
	})(jQuery)
</script>
<div id="app">
	<component :is="currentView"></component>
</div>
@endsection