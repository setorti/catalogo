@extends('main')
@section("content")
<center>
	<div class="card standard-form register-form">
		<div class="card-header">
			Atualizar Informações
		</div>
		<div class="card-body">
			{!! Form::open(['url' => "/users/update/{$user->id}", 'method' => 'post']) !!}
			<div class="form-group">
				<label for="name">Novo Nome</label>
                <input class="form-control" name="name" value="{{ $user->name }}" required>
			</div>
			<div class="form-group">
				<label for="password">Nova Senha</label>
				<input type="password" class="form-control" name="password" placeholder="deixe em branco se não deseja alterar">
			</div>
			<div class="form-group">
				<button class="btn btn-black">Atualizar</button>
			</div>
			{!! Form::close() !!}
		</div>
	</div>
</center>
@endsection