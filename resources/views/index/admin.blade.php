@extends('main')
@section("content")
<script type="text/javascript">
    (function($) {
        $(document).ready(function() {
            /* ------- COMPONENTS -------------- */
            Vue.component('files', { // OK
                template: "#files-template",
                data: function() {
                    return {
                        currentFile: [],
                        files: [],
                        tags: [],
                        fileform: {
                            title: '',
                            description: '',
                            cover: '',
                            url: '',
                            tags: [],
                        },
                        params: {
                            limit: {{ $limit }},
                            page: 1,
                        },
                    }
                },
                watch: {
                    currentFile: function() {
                        this.fileform.title = this.currentFile.title;
                        this.fileform.description = this.currentFile.description;
                        this.fileform.cover = '';
                        this.fileform.url = this.currentFile.url;
                        this.fileform.tags = this.currentFile.file_tags.map(tag => tag.tag_id);

                        this.clearFileInput();
                    }
                },
                methods: {
                    clearFileForm: function() {
                        this.fileform = {
                            title: '',
                            description: '',
                            cover: '',
                            url: '',
                            tags: []
                        };
                    },
                    clearFileInput: function() {
                        $("input[type='file']").val('');
                    },
                    getFormData: function(object) {
                        let fileform = new FormData();
                        for(let key in object) {
                            fileform.append(key, object[key]);
                        }
                        return fileform;
                    },
                    pageClickHandler: function(page) {
                        if( page === this.params.page ) return;
                        this.params.page = page;
                        this.fetchFiles();
                    },
                    fetchFiles: function() {
                        $.get("{{ URL::to('/api/files') }}", { page: this.params.page, limit: this.limit }, files => {
                            this.files = files;
                            this.params.page = this.files.current_page;
                            $("body").scrollTo("#app", 800, {offset: -100});
                        });
                    },
                    fileInputHandler: function(event) {
                        this.fileform.cover = event.target.files[0];
                        console.log(Object.values(this.fileform.cover));
                    },
                    fileStoreHandler: function() {
                        var fileform = this.getFormData(this.fileform);
                        $.ajax({
                            type: "POST",
                            data: fileform,
                            url: `{{ URL::to('/api/files/store') }}`,
                            processData: false,
                            contentType: false,
                            success: this.fileAddSuccessCallback,
                        });
                    },
                    fileUpdateHandler: function(id) {
                        var fileform = this.getFormData(this.fileform);
                        $.ajax({
                            type: "POST",
                            data: fileform,
                            url: `{{ URL::to('/api/files/update/${id}') }}`,
                            processData: false,
                            contentType: false,
                            success: this.fileUpdateSuccessCallback,
                        });
                    },
                    fileUpdateHandlerLite: function(file) {
                        $.post(`{{ URL::to('/api/files/update/${file.id}') }}`, { title: file.title, description: file.description, url: file.url }, response => {
                            // response...
                        });
                    },
                    fileDeleteHandler: function(id) {
                        let file = this.files.data.filter(f => f.id === id);
                        if(file.length) {
                            file = file[0];
                            let assert = confirm(`Deseja apagar o arquivo ${file.title} (ID: ${file.id}) ?`);
                            if(assert) {
                                $.post(`{{ URL::to('/api/files/delete') }}`, {id: id}, response => {
                                    if(response) {
                                        alert("Arquivo apagado com sucesso.");
                                        this.fetchFiles();
                                        return;
                                    }
                                    alert("Falha ao apagar arquivo.");
                                });
                            }
                        }
                    },
                    fileAddSuccessCallback: function(insertedFile) {
                        if(insertedFile)
                        {
                            this.fetchFiles();
                            this.clearFileInput();
                            $("#file-add-modal").modal('hide');
                            alert("Arquivo adicionado com sucesso.");
                            return;
                        }

                        alert("Falha ao enviar arquivo.");
                    },
                    fileUpdateSuccessCallback: function(updatedFile) {
                        if(updatedFile)
                        {
                            this.fetchFiles();
                            this.clearFileInput();
                            alert("Arquivo atualizado com sucesso.");
                            return;
                        }

                        alert("Falha ao atualizar informações do arquivo.");
                    },
                    fileAddHandler: function() {
                        this.clearFileForm();
                        $("#file-add-modal").modal();
                    },
                    fileEditHandler: function(id) {
                        var result = this.files.data.filter(file => file.id === id);
                        if(result.length > 0) {
                            this.currentFile = result[0];
                            $("#file-edit-modal").modal();
                        }
                    },
                },
                mounted: function() {
                    $.get("{{ URL::to('/api/files') }}", { page: this.params.page, limit: this.params.limit }, files => {
                        this.files = files;
                    });
                    $.get("{{ URL::to('/api/tags') }}", tags => this.tags = tags);
                }
            });
            Vue.component('download-ranking', { // OK
                extends: VueChartJs.Bar,
                data: function() {
                    return {
                        users: [],
                    };
                },
                methods: {

                },
                mounted: function() {
                    $.get("{{ URL::to('/api/users') }}", {order: "total_downloads", direction: "desc"}, users => {
                        this.users = users;

                        let labels = this.users.map(user => user.name);
                        let downloads = this.users.map(user => user.total_downloads);

                        this.renderChart({
                            labels: labels,
                            datasets: [
                                {
                                    label: 'Downloads',
                                    backgroundColor: '#26CEFF',
                                    data: downloads,
                                }
                            ],
                        }, {
                            animation: false,
                            responsive: true,
                            legend: {
                                display: false
                            },
                            scales:
                            {
                                xAxes: [{
                                    display: false
                                }]
                            }
                        });
                    });
                }
            });
            Vue.component('tags', { // OK
                template: "#tags-template",
                data: function() {
                    return {
                        currentTag: [],
                        tagform: {
                            name: '',
                        },
                        tags: [],
                    };
                },
                watch: {
                    currentTag: function() {
                        this.tagform.name = this.currentTag.name;
                    },
                },
                methods: {
                    clearTagForm: function() {
                        this.tagform = {
                            name: ''
                        };
                    },
                    fetchTags: function() {
                        $.get("{{ URL::to('/api/tags') }}", tags => this.tags = tags);
                    },
                    tagAddHandler: function() {
                        this.clearTagForm();
                        $("#tag-add-modal").modal();
                    },
                    tagEditHandler: function(id) {
                        if(this.currentTag.id != id) {
                            let result = this.tags.filter(tag => tag.id === id);
                            if(result.length) {
                                this.currentTag = result[0];
                            }
                        }

                        $("#tag-edit-modal").modal();
                    },
                    tagDeleteHandler: function(id) {
                        let del = confirm("Deseja apagar esta Tag?");
                        if(del) {
                            $.post("{{ URL::to('/api/tags/delete/') }}", {id: id}, response => {
                                if(response) {
                                    this.fetchTags();
                                    alert("Tag apagada com sucesso.");
                                    return;
                                }

                                alert("Falha ao apagar Tag. Verifique se a Tag não está sendo utilizada por algum arquivo.");
                            });
                        }
                    },
                    tagUpdateHandler: function() {
                        let id = this.currentTag.id;
                        $.post(`{{ URL::to('/api/tags/update/${id}') }}`, this.tagform, response => {
                            if(response) {
                                this.fetchTags();
                                alert("Tag atualizada com sucesso.");
                                $("#tag-edit-modal").modal("hide");
                                return;
                            }

                            alert("Falha ao atualizar Tag.");
                        });
                    },
                    TagStoreHandler: function() {
                        $.post("{{ URL::to('/api/tags/store') }}", this.tagform, response => {
                            if(response) {
                                this.fetchTags();
                                alert("Tag adicionada com sucesso.");
                                $("#tag-add-modal").modal("hide");
                                return;
                            }

                            alert("Falha ao adicionar Tag.");
                        });
                    },
                },
                mounted: function() {
                    this.fetchTags();
                }
            });
            Vue.component('users', { // OK
                template: "#users-template",
                data: function() {
                    return {
                        filesDownloadLimit: 0,
                        users: [],
                        activationOptions: [
                            { value: "-1", label: "Pendente Administrador"},
                            { value: "0", label: "Pendente Email"},
                            { value: "1", label: "Ativo"},
                        ],
                    }
                },
                methods: {
                    fetchUsers: function() {
                        $.get("{{ URL::to('/api/users') }}", users => this.users = users);
                    },
                    fetchDownloadLimit: function() {
                        $.get("{{ URL::to('/api/settings/get') }}", {key: 'files_download_limit'}, response => {
                            if(response) {
                                this.filesDownloadLimit = response.value;
                            }
                        });
                    },
                    toggleActivationStatus: function(user) {
                        $.post(`{{ URL::to('/api/users/toggle/activation/${user.id}') }}`, {activation: user.activation}, response => {
                            console.log(response);
                        });
                    },
                    toggleAdmin: function(id) {
                        $.get( `{{ URL::to('/api/users/toggle/admin/${id}') }}`, response => {
                            if(response) {
                                alert(`Preferências de administrador alteradas para o usuário id ${id}.`);
                            } else {
                                alert("Falha ao atualizar preferências.");
                            }

                            this.fetchUsers();
                        });
                    },
                    userDeleteHandler: function(user) {
                        let assert = confirm(`Deseja apagar o usuário ${user.name} (ID: ${user.id}) ?`);
                        if(assert) {
                            $.get( `{{ URL::to('/api/users/delete/${user.id}') }}`, response => {
                                if(response) {
                                    alert(`Usuário deletado.`);
                                } else {
                                    alert("Falha ao deletar usuário.");
                                }

                                this.fetchUsers();
                            });
                        }
                    },
                    eraseDownloads: function(id) {
                        $.get( `{{ URL::to('/api/users/erase/downloads/${id}') }}`, response => {
                            if(response) {
                                this.fetchUsers();
                            } else {
                                alert(`Falha ao zerar downloads do usuário id ${id}.`);
                            }
                        });
                    }
                },
                mounted: function() {
                    this.fetchUsers();
                    this.fetchDownloadLimit();
                }
            });
            Vue.component('settings', { // OK
                template: "#settings-template",
                data: function() {
                    return {
                        settings: [],
                    }
                },
                methods: {
                    fetchSettings: function() {
                        $.get("{{ URL::to('/api/settings') }}", response => {
                            this.settings = response;
                            console.log(this.settings);
                        });
                    },
                    settingUpdateHandler: function() {
                        $.post(`{{ URL::to('/api/settings/update-all') }}`, {settings: this.settings}, response => {
                            if(response) {
                                alert(`Preferências atualizadas com sucesso!`);
                            } else {
                                alert("Falha ao atualizar preferências.");
                            }
                        });
                    },
                },
                mounted: function() {
                    this.fetchSettings();
                    setTimeout(function() {
                        tinymce.init({
                            selector:'textarea.tinymce',
                            height: 400,
                            theme: 'modern',
                            plugins: 'print preview fullpage searchreplace autolink directionality visualblocks visualchars fullscreen image link media template codesample table charmap hr pagebreak nonbreaking anchor toc insertdatetime advlist lists textcolor wordcount imagetools contextmenu colorpicker textpattern help',
                        });
                }, 500);
                }
            })
            /* --------------------------------- */
            var app = new Vue({
                el: "#app",
                data: {
                    currentView: 'files',
                    files: [],
                },
                methods: {
                    toggleView(view) {
                        if(this.currentView !== view) {
                            this.currentView = view;
                        }
                    }
                },
                beforeMount: function() {

                }
            });
        })
    })(jQuery)
</script>
<script type="application/x-template" id="files-template">
    <div class="files-component">
        <div class="form-group float-sm-right">
            <button v-on:click="fileAddHandler" class="btn btn-info">Adicionar Arquivo <i class="fas fa-file"></i></button>
        </div>
        <table class="table table-bordered table-responsive-sm">
            <thead>
                <tr>
                    <td class="text-center">#</td>
                    <td class="text-center">Imagem</td>
                    <td class="text-center">Nome</td>
                    <td class="text-center">Conteúdo</td>
                    <td class="text-center">Tags</td>
                    <td class="text-center">Editar/Remover</td>
                </tr>
            </thead>
            <tbody>
                <tr v-for="file in files.data">
                    <td class="text-center">@{{ file.id }}</td>
                    <td class="text-center"><img class="img-responsive" v-bind:style="{width: '100px', height: '100px', 'object-fit': 'cover'}" v-bind:src="'{{ URL::asset('storage/cover') }}' + '/' + file.cover"></td>
                    <td class="text-center"><textarea class="form-control" v-on:keyup="fileUpdateHandlerLite(file)" v-model="file.title" v-bind:style="{'min-width': '200px', 'min-height': '100px'}"></textarea></td>
                    <td class="text-center"><textarea class="form-control" v-on:keyup="fileUpdateHandlerLite(file)" v-model="file.description" v-bind:style="{'min-width': '200px', 'min-height': '100px'}"></textarea></td>
                    <td class="text-center">
                        <div v-if="!file.file_tags.length"><i>Sem Tags. <i v-bind:style="{color: 'red'}" class="fas fa-ban"></i></i></div>
                        <div v-else v-for="fileTag in file.file_tags">- @{{ fileTag.tag.name }}</div>
                    </td>
                    <td class="text-center">
                        <button class="btn btn-info" v-on:click="fileEditHandler(file.id)"><i class="far fa-edit"></i></button>
                        <button class="btn btn-danger" v-on:click="fileDeleteHandler(file.id)"><i class="far fa-trash-alt"></i></button>
                    </td>
                </tr>
            </tbody>
        </table>
        <ul class="pagination justify-content-center" v-if="files.length > 0 || Object.keys(files).length > 0">
            <li v-for="page in files.last_page" class="page">
                <a class="page-link" v-bind:class="{ 'current-page' : page == params.page }" v-on:click="pageClickHandler(page)">@{{ page }}</a>
            </li>
        </ul>
        <div class="modal" tabindex="-1" role="dialog" id="file-add-modal"> <!-- File Add Modal -->
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Adicionar Arquivo</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                        <form method="post" v-on:submit.prevent="fileStoreHandler">
                            <div class="form-group">
                                <label for="title">Nome do arquivo</label>
                                <input name="text" class="form-control" v-model="fileform.title" required="true">
                            </div>
                            <div class="form-group">
                                <label for="url">Link do arquivo</label>
                                <input class="form-control" v-model="fileform.url" required="true">
                            </div>
                            <div class="form-group">
                                <label for="image">Imagem de capa</label>
                                <input type="file" class="form-control" v-on:change="fileInputHandler" enctype='multipart/form-data' required="true">
                            </div>
                            <div class="form-group">
                                <label for="content">Conteúdo</label>
                                <textarea class="form-control" v-model="fileform.description" required="true"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="tags">Tags</label>
                                <div class="form-check" v-for="(tag, index) in tags">
                                    <input class="form-check-input" type="checkbox" v-model="fileform.tags" v-bind:value="tag.id">
                                    <label class="form-check-label">@{{ tag.name }}</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Enviar Arquivo</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal" tabindex="-1" role="dialog" id="file-edit-modal"> <!-- File Edit Modal -->
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Edição de Arquivo - @{{ currentFile.title }} (ID: @{{ currentFile.id }})</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                        <form method="post" v-on:submit.prevent="fileUpdateHandler(currentFile.id)">
                            <div class="form-group">
                                <label for="title">Nome do arquivo</label>
                                <input name="text" class="form-control" v-model="fileform.title">
                            </div>
                            <div class="form-group">
                                <label for="url">Link do arquivo</label>
                                <input class="form-control" v-model="fileform.url" required="true">
                            </div>
                            <div class="form-group">
                                <label for="image">Imagem de capa</label>
                                <input type="file" class="form-control" v-on:change="fileInputHandler" enctype='multipart/form-data'>
                            </div>
                            <div class="form-group">
                                <label for="content">Conteúdo</label>
                                <textarea class="form-control" v-model="fileform.description"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="tags">Tags</label>
                                <div class="form-check" v-for="(tag, index) in tags">
                                    <input class="form-check-input" type="checkbox" v-model="fileform.tags" v-bind:value="tag.id">
                                    <label class="form-check-label">@{{ tag.name }}</label>
                                </div>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Salvar Dados</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>
<script type="application/x-template" id="tags-template">
    <div class="tags-component">
        <div class="form-group float-sm-right">
            <button v-on:click="tagAddHandler" class="btn btn-info">Adicionar Tag <i class="fas fa-tag"></i></button>
        </div>
        <table class="table table-bordered table-responsive-sm">
            <thead>
                <tr>
                    <td class="text-center">#</td>
                    <td class="text-center">Nome</td>
                    <td class="text-center">Editar/Remover</td>
                </tr>
            </thead>
            <tbody>
                <tr v-for="tag in tags">
                    <td class="text-center">@{{ tag.id }}</td>
                    <td class="text-center">@{{ tag.name }}</td>
                    <td class="text-center">
                        <button class="btn btn-info" v-on:click="tagEditHandler(tag.id)"><i class="far fa-edit"></i></button>
                        <button class="btn btn-danger" v-on:click="tagDeleteHandler(tag.id)"><i class="far fa-trash-alt"></i></button>
                    </td>
                </tr>
            </tbody>
        </table>
        <div class="modal" tabindex="-1" role="dialog" id="tag-add-modal"> <!-- Tag Add Modal -->
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Adicionar Tag</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                        <form method="post" v-on:submit.prevent="TagStoreHandler">
                            <div class="form-group">
                                <label for="title">Nome da tag</label>
                                <input name="text" class="form-control" v-model="tagform.name" required="true">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Adicionar Tag</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal" tabindex="-1" role="dialog" id="tag-edit-modal"> <!-- Tag Edit Modal -->
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                      <h5 class="modal-title">Edição de Tag - @{{ currentTag.name }} (ID: @{{ currentTag.id }})</h5>
                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                      </button>
                    </div>
                    <div class="modal-body">
                        <form method="post" v-on:submit.prevent="tagUpdateHandler">
                            <div class="form-group">
                                <label for="title">Nome da tag</label>
                                <input name="text" class="form-control" v-model="tagform.name" required="true">
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-primary">Salvar Tag</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</script>
<script type="application/x-template" id="users-template">
    <div class="users-component">
        <table class="table table-bordered table-responsive-sm">
            <thead>
                <tr>
                    <td class="text-center">#</td>
                    <td class="text-center">Usuário</td>
                    <td class="text-center">Acesso</td>
                    <td class="text-center">Zerar Downloads</td>
                    <td class="text-center">Administrador</td>
                    <td class="text-center">Apagar</td>
                </tr>
            </thead>
            <tbody>
                <tr v-for="user in users">
                    <td class="text-center">@{{ user.id }}</td>
                    <td class="text-center">@{{ user.name }} ( <b>@{{ user.email }}</b> )</td>
                    <td class="text-center">
                        <select class="form-control" v-model="user.activation" v-on:change="toggleActivationStatus(user)" v-bind:disabled="user.activation == 0">
                            <option v-for="option in activationOptions" v-bind:value="option.value" :selected="option.value === user.activation" v-bind:disabled="option.value == 0">@{{ option.label }}</option>
                        </select>
                    </td>
                    <td class="text-center"><button v-on:click="eraseDownloads(user.id)" class="btn btn-info">ZERAR (@{{user.downloads}}/@{{filesDownloadLimit}})</button></td>
                    <td class="text-center"><input class="form-control" v-on:change="toggleAdmin(user.id)" type="checkbox" v-bind:checked="user.admin == 1"></td>
                    <td class="text-center">
                        <button class="btn btn-danger" v-on:click="userDeleteHandler(user)"><i class="far fa-trash-alt"></i></button>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
</script>
<script type="application/x-template" id="settings-template">
    <div class="settings-component">
        <form v-on:submit.prevent="settingUpdateHandler" method="POST">
            <div class="setting form-group" v-if="settings.files_download_limit !== undefined">
                <label for="files_download_limit">Limite Diário de Downloads</label>
                <input class="form-control" type="number" min="1" required="true" v-model="settings.files_download_limit.value">
            </div>
            <div class="setting form-group" v-if="settings.files_download_limit !== undefined">
                <label for="files_download_limit">Número de Itens por Página (Paginação)</label>
                <input class="form-control" type="number" min="1" required="true" v-model="settings.files_pagination.value">
            </div>
            <div class="setting form-group" v-if="settings.about_text !== undefined">
                <label for="about_text">Texto do "Sobre o Catálogo"</label>
                <textarea class="form-control tinymce" v-model="settings.about_text.value"></textarea>
            </div>
            <div class="form-group">
                <button type="submit" class="btn btn-info">Salvar</button>
            </div>
        </form>
    </div>
</script>
<div id="app">
    <ul class="nav nav-tabs panel-switcher">
        <li class="nav-item">
            <a class="nav-link" v-on:click="toggleView('files')" v-bind:class="{ active: currentView == 'files' }" href="#">Arquivos</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" v-on:click="toggleView('download-ranking')" v-bind:class="{ active: currentView == 'download-ranking' }" href="#">Downloads por usuário</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" v-on:click="toggleView('tags')" v-bind:class="{ active: currentView == 'tags' }" href="#">Tags</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" v-on:click="toggleView('users')" v-bind:class="{ active: currentView == 'users' }" href="#">Usuários</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" v-on:click="toggleView('settings')" v-bind:class="{ active: currentView == 'settings' }" href="#">Configurações</a>
        </li>
    </ul>
    <component :is="currentView"></component>
</div>
@endsection