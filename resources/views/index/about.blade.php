@extends('main')
@section("content")
	<?php echo htmlspecialchars_decode($about_text); ?>

    @if( Auth::check() )

    <hr>

	{!! Form::open(['action' => 'FormsController@about', 'method' => 'post']) !!}
    <div class="form-group">
    	<label for="email">Seu Email</label>
        <input type="email" class="form-control" name="email" required>
    </div>
    <div class="form-group">
    	<label for="subject">Solicitação</label>
        <textarea class="form-control" style="height: 200px;" minlength="6" maxlength="500" name="request" required></textarea>
    </div>
    <div class="form-group">
	    <button type="submit" class="btn btn-black">Enviar <i class="fa fa-upload"></i></button>
    </div>
    {!! Form::close() !!}

    @endif
@endsection