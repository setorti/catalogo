<!DOCTYPE html>
<html lang="{{ Lang::locale() }}">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<title>Catálogo de Comunicação Shalom</title>
	@include('partials/scripts')
</head>

<body>
	<nav class="navbar navbar-expand-lg" id="navbar"> <!-- Main Navbar -->
		<a class="navbar-brand" href="{{URL::to('/welcome')}}"><img class="img-responsive header-logo" src="{{ URL::asset('img/logo.svg') }}"></a>
		<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbar-collapse" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
			<i class="fa fa-bars" aria-hidden="true"></i>
		</button>

		<div class="collapse navbar-collapse" id="navbar-collapse">
		
@if(Auth::check())
			<ul class="navbar-nav ml-5 align-items-center w-100 menu">
				<!-- <li class="nav-item item">
					<a class="nav-link" href="{{ URL::to('/login') }}">
					<i class="fa fa-home" aria-hidden="true"></i> 
					<span>bem vindo,</span>
					<span>pode entrar!</span>
					</a>
				</li> -->
				<li class="nav-item item">
					<a class="nav-link" href="{{ URL::to('/institucional') }}">
					<span>material</span>
					<span>institucional</span>
					</a>
				</li>
				<li class="nav-item item">
					<a class="nav-link" href="{{ URL::to('/artes') }}"> <span>artes para</span> <span>download</span></a>
				</li>
				<li class="nav-item item">
					<a class="nav-link" href="{{ URL::to('/banco') }}"> <span>banco</span> <span>de fotos</span></a>
				</li>
				<li class="nav-item item">
					<a class="nav-link" href="{{ URL::to('/campanhas') }}"> <span>Campanhas</span> <span>liberadas</span></a>
				</li>
				<li class="nav-item item item-workshops">
					<a class="nav-link" href="https://www.youtube.com/channel/UCb4XQstC8HxrIgmCk2x1rAQ"> <span>Workshops</span></a>
				</li>
				<li>
					<a class="fab fa-instagram" href="https://www.instagram.com/criacaoshalom/"> Instagram
					</a>
				</li>
			</ul>
			<ul class="nav-login">
			<li class="nav-item dropdown ml-5">
					<a href="javascript:void(0)" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="fa fa-user"></i> {{ Auth::user()->name }}
						<div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
							<a class="dropdown-item" href="{{ url('users/edit', Auth::user()['id']) }}"><i class="fa fa-user"></i> Meu Perfil</a>
							@if(Auth::user()['admin'])
							<a class="dropdown-item" href="{{ url('/admin') }}"><i class="fas fa-columns"></i> Painel de Administrador</a>
							@endif
							<a class="dropdown-item" href="{{ URL::to('/logout') }}"><i class="fa fa-sign-out"></i> Sair</a>
						</div>
					</a>
				</li>
			</ul>
@else
			<ul class="navbar-nav ml-5  w-100 menu">
				<li class="nav-item item">
						<a class="nav-link" href="{{ URL::to('/login') }}">
						<!-- <i class="fa fa-home" aria-hidden="true"></i>  -->
						<span>bem vindo,</span>
						<span>pode entrar!</span>
						</a>
				</li>
			</ul>
@endif
			<!-- @include("partials/menu-right") -->
		</div>
	</nav>
	<nav class="notificator">@include('partials/notificator')</nav>
	<div class="container-fluid content">
		<div class="page-{{ explode('@', \Route::currentRouteAction())[1] }}">
			@yield("content")
		</div>
	</div>
</body>
</html>