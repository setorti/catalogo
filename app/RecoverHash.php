<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RecoverHash extends Model
{
    protected $table = 'password_recover';
    protected $fillable = [
        'user_id',
        'hash',
        'expires',
    ];

    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
