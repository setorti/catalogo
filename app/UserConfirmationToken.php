<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserConfirmationToken extends Model
{
    protected $table = 'user_confirmation_tokens';
    protected $fillable = [
    	'user_id',
    	'token',
    	'expiration',
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }
}
