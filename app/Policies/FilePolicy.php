<?php

namespace App\Policies;

use App\User;
use App\File;
use App\Setting;
use Illuminate\Auth\Access\HandlesAuthorization;

class FilePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function download(User $user, File $file)
    {
        $max_downloads = Setting::where(['key' => 'files_download_limit'])->firstOrFail();
        return $user->admin == 1 || $user->downloads < $max_downloads->value;
    }
}
