<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function activation(User $user, User $target)
    {
        return $user->admin == 1 && $user->id != $target->id;
    }

    public function downloads(User $user, User $target)
    {
        return $user->admin == 1 && $user->id != $target->id;
    }

    public function admin(User $user, User $target)
    {
        return $user->admin == 1 && $user->id != $target->id;
    }

    public function edit(User $user, User $target)
    {
        return $user->admin == 1 || $user->id == $target->id;
    }

    public function update(User $user, User $target)
    {
        return $user->admin == 1 || $user->id == $target->id;
    }

    public function delete(User $user, User $target)
    {
        return $user->admin == 1 && $user->id != $target->id && $target->admin == 0;
    }
}
