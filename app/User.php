<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'activation', 'admin', 'total_downloads', 'absolute_downloads', 'download_expire',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static $validators = [
        'add' => [
            [
                'name' => 'required|min:6|max:50',
                'email' => 'required|email|max:50|unique:users,email|regex:/[a-zA-Z]+[a-zA-Z0-9]*\@comshalom\.org/',
                'password' => 'required|min:6|max:20',
            ],
            [
                'name.min' => 'O nome deve conter pelo menos :min caracteres',
                'name.max' => 'O nome deve conter até :max caracteres',
                'email.unique' => 'O email solicitado já foi cadastrado.',
                'email.regex' => 'O email de cadastrado deve ser do domínio comshalom.org.'
            ],
        ],
        'update' => [
            'name' => 'required|min:6|max:50',
            'password' => 'min:6|max:20',
        ],
        'login' => [
            'email' => 'required|email|max:50',
            'password' => 'required|min:6|max:20',
        ],
    ];

    public static function register_validate($data)
    {
        $validator = \Validator::make($data, [
            'name' => 'required|min:6|max:50',
            'email' => 'required|email|max:50|unique:users,email|regex:/[a-zA-Z]+[a-zA-Z0-9]*\@comshalom\.org/',
            'password' => 'required|min:6|max:20',
        ], [
            'name.min' => 'O nome deve conter pelo menos :min caracteres',
            'name.max' => 'O nome deve conter até :max caracteres',
            'email.unique' => 'O email solicitado já foi cadastrado.',
            'email.regex' => 'O email cadastrado deve ser do domínio comshalom.org.'
        ]);
        return $validator;
    }

    public static function update_validate($data) {
        $validator = \Validator::make($data, [
            'name' => 'required|min:6|max:50',
            'password' => 'min:6|max:20',
        ]);
        return $validator;
    }

    public static function login_validate($data)
    {
        $validator = \Validator::make($data, [
            'email' => 'required|email|max:50',
            'password' => 'required|min:5|max:20',
        ]);
        return $validator;
    }
}
