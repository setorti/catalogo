<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\FileTag;
use DB;

class File extends Model
{
    protected $fillable = [
        'title',
        'description',
        'url',
        'cover',
        'user_id',
    ];

    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function file_tags()
    {
    	return $this->hasMany('App\FileTag');
    }

    public static function validate($data)
    {
    	$validator = \Validator::make($data, [
            'user_id' => 'required|integer|exists:users,id',
    		'title' => 'required|max:100',
    		'tags' => 'min:0|exists:tags,id',
    		'description' => 'required|max:200',
    		'cover' => 'required|image|max:4096',
    		'url' => 'required|url',
    	]);
    	return $validator;
    }

    public static function update_validate($data)
    {
        $validator = \Validator::make($data, [
            'title' => 'required|max:100',
            'tags' => 'min:0|exists:tags,id',
            'description' => 'required|max:200',
            'cover' => 'nullable|image|max:4096',
            'url' => 'required|url',
        ]);
        return $validator;
    }

    public static function search( $terms )
    {
        $files = [ ];
        $filtered = [ ];

        foreach( File::orderBy('title', 'ASC')->get() as $file )
        {
            $id = $file->id;
            // Inicializando Index.
            $files[ $id ] = [ ];
            // Indexando Titulo do arquivo.
            $files[ $id ][ ] = $file->title;

            // Indexando palavras da descrição do arquivo.
            $files[ $id ] = array_map('strtolower', array_merge( $files[ $id ], explode(' ', $file->description ) ) );

            // Indexando Tags.
            foreach( $file->file_tags as $file_tag )
            {
                $files[ $id ][ ] = strtolower( $file_tag->tag->name );
            }

            // Iterando no Index do arquivo.
            foreach( $files[ $id ] as $file_term )
            {
                // Evitando duplicada de arquivos.
                if( in_array( $file, $filtered ) ) break;

                // Verificando existência do termo buscado no Index.
                if( in_array( $file_term, $terms ) )
                {
                    $filtered[ ] = $file;
                    continue;
                }

                // Verificando Substrings em termos buscados.
                foreach( $terms as $term )
                {
                    if( stripos( $file_term, $term ) !== FALSE )
                    {
                        $filtered[ ] = $file;
                        continue;
                    }
                }
            }
        }

        return $filtered;
    }
}
