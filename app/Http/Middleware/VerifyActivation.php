<?php

namespace App\Http\Middleware;

use Auth;
use Closure;
use App\UserConfirmationToken;
use App\RecoverHash;
use App\User;
use Carbon\Carbon;

/**
 *  User 'activation' status:
 *
 *  -1  ====>   User Needs Email Activation.
 *  0   ====>   User Needs Admin Activation.
 *  1   ====>   User Without Restrictions.
 */
class VerifyActivation
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $userConfirmationTokens = UserConfirmationToken::orderBy('id', 'ASC')->get();
        $recovers = RecoverHash::orderBy('id', 'ASC')->get();

        $now = Carbon::now();

        foreach( $userConfirmationTokens as $userConfirmationToken )
        {
            if($now->timestamp > strtotime($userConfirmationToken->expiration))
            {
                $userConfirmationToken->delete();
            }
        }

        foreach( $recovers as $recover )
        {
            if($now->timestamp > strtotime($recover->expires))
            {
                $recover->delete();
            }
        }

        if(Auth::check())
        {
            $user_id = Auth::user()['id'];

            $user_admin = Auth::user()['admin'];

            $user = User::findOrFail($user_id);

            $activation = intval( $user['activation'] );

            $admin = $request->route()->getAction()['admin'] ?? FALSE;

            switch($activation)
            {
                case -1: // User Needs ADMINISTRATOR Activation.
                {
                    return response()->view('errors/denied');
                }
                case 0: // User Needs EMAIL Activation.
                {
                    return response()->view('errors/mail-activation');
                }
                default: break;
            }

            if( $user->download_expire != null && $now->timestamp > strtotime($user->download_expire) )
            {
                $user->downloads = 0;
                $user->download_expire = null;
                $user->save();
            }

            if( $admin && ! $user_admin ) {
                return redirect()->to('/')->with(['message' => 'Acesso Negado.', 'type' => 'danger']);
            }
        }

        return $next($request);
    }
}
