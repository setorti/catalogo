<?php

namespace App\Http\Controllers;

use App\Http\Repository\SettingsRepository;
use Illuminate\Http\Request;

class SettingsController extends Controller
{
    private $repository;

    public function __construct()
    {
        if( $this->repository == null )
        {
            $this->repository = new SettingsRepository();
        }
    }

    public function index()
    {
        return response()->json($this->repository->all());
    }

    public function get(Request $request)
    {
        $data = $request->all();
        $key = $data['key'] ?? 'null';

        return response()->json($this->repository->by_key($key));
    }

    public function update_all(Request $request)
    {
        $data = $request->all();

        $settings = $data['settings'];

        return response()->json($this->repository->update_all($settings));
    }
}
