<?php

namespace App\Http\Controllers;

use Auth;
use Gate;
use App\RecoverHash;
use App\User;
use App\UserConfirmationToken;
use Illuminate\Http\Request;

use App\Http\Repository\UsersRepository;

class UsersController extends Controller
{
    private $repository;

    public function __construct()
    {
        if( $this->repository == null )
        {
            $this->repository = new UsersRepository();
        }
    }

    public function index(Request $request)
    {
        $data = $request->all();

        $field = $data['order'] ?? null;

        $direction = $data['direction'] ?? null;

        return response()->json($this->repository->order( $field, $direction ));
    }

    public function edit($id)
    {
        $user = User::findOrFail($id);

        if( Gate::denies('edit', $user) )
        {
            return redirect()->to('/');
        }

        return view("users/edit", compact("user"));
    }

    public function delete($id)
    {
        return response()->json($this->repository->delete($id));
    }

    public function update($id, Request $request)
    {
        $data = $request->all();

        $user = User::findOrFail($id);

        if( Gate::denies('edit', $user) )
        {
            return redirect()->to('/');
        }

        if( $this->repository->update($id, $data) )
        {
            return redirect()->to('/')->with(['message' => 'Informações de usuário atualizadas com sucesso.', 'type' => 'success']);
        }

        return redirect()->back()->with(['message' => 'Falha ao atualiar informações de usuário.', 'type' => 'error']);
    }

    public function login()
    {
        return view("users/login");
    }

    public function welcome()
    {
        return view("users/welcome");
    }

    public function toggle_admin($id)
    {
        return response()->json($this->repository->toggle_admin($id));
    }

    public function toggle_activation($id, Request $request)
    {
        $data = $request->all();

        $activation = $data['activation'];

        return response()->json($this->repository->toggle_activation($id, $activation));
    }

    public function erase_downloads($id)
    {
        return response()->json($this->repository->erase_downloads($id));
    }

    public function logout()
    {
        if(Auth::check())
        {
            Auth::logout();

            return redirect()->to('/')->with(['message' => 'Deslogado com sucesso!', 'type' => 'success']);
        }

        return redirect()->to('/');
    }

    public function register()
    {
        return view("users/register");
    }

    public function confirm($token)
    {
        $confirmation = UserConfirmationToken::where('token', '=', $token)->firstOrFail();

        if( $this->repository->confirm( $confirmation ) ) {
            return redirect()->to('/')->with(['message' => 'Conta ativada com sucesso!', 'type' => 'success']);
        }

        return redirect()->to('/');
    }
}
