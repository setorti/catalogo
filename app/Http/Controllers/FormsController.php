<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Repository\FormsRepository;

class FormsController extends Controller
{
    private $repository;

    public function __construct() {
        if( $this->repository == null ){
            $this->repository = new FormsRepository();
        }
    }

    public function about(Request $request)
    {
        $data = $request->all();

        if( $this->repository->send($data) ) {
            return redirect()->to('/about')->with(['message' => 'Solicitação enviada com sucesso.', 'type' => 'success']);
        }

        return redirect()->to('/about')->with(['message' => 'Falha ao enviar solicitação', 'type' => 'danger']);
    }
}
