<?php

namespace App\Http\Controllers;

use App\RecoverHash;
use App\Http\Repository\UsersRepository;
use Illuminate\Http\Request;

class RecoverHashController extends Controller
{
    private $users_repository;

    public function  __construct()
    {
        if( $this->users_repository == null )
        {
            $this->users_repository = new UsersRepository();
        }
    }

    public function recover($hash)
    {
        $recover_hash = RecoverHash::where('hash', '=', $hash)->firstOrFail();
        return view('users/recover', compact("recover_hash"));
    }

    public function request(Request $request)
    {
        $data = $request->all();

        return response()->json($this->users_repository->request_recover($data));
    }

    public function password(Request $request)
    {
        $data = $request->all();

        if( $this->users_repository->recover_password($data) )
        {
            return redirect()->to('/')->with(['message' => 'Senha alterada com sucesso.', 'type' => 'success']);
        }

        return redirect()->to('/');
    }
}
