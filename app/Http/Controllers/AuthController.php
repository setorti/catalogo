<?php

namespace App\Http\Controllers;

use App\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Facade\UserFacade;
use App\Http\Repository\UsersRepository;
use App\Http\Validators\UserValidator;

class AuthController extends Controller
{
	private $repository;

	public function __construct()
    {
        if( $this->repository == null )
        {
            $this->repository = new UsersRepository();
        }
    }

	public function register(Request $request)
	{
		$data = $request->all();

		$userFacade = new UserFacade();

		$result = $userFacade->register($data);

		if( $result['success'] === FALSE || $result === FALSE )
		{
			return redirect()->back()->with(['message' => $result['errors'], 'type' => 'danger']);
		}

		return redirect()->to('/login')->with(['message' => 'Cadastro realizado com sucesso. Verifique seu email.', 'type' => 'success']);
	}

    public function login(Request $request)
    {
    	$data = $request->all();

		$result = $this->repository->login($data);

		if( $result['success'] === TRUE )
		{
			return redirect()->to('/welcome')->with(['message' => 'Autenticado com sucesso.', 'type' => 'success']);
		}

		if( !empty($result['errors']) )
		{
			return redirect()->back()->with(['message' => $result['errors'], 'type' => 'danger']);
		}

		return redirect()->back()->with(['message' => 'Email ou senha inválidos.', 'type' => 'danger']);
    }
}
