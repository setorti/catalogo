<?php

namespace App\Http\Controllers;

use Auth;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use App\Http\Repository\FilesRepository;
use App\Http\Repository\SettingsRepository;
use App\File;
use App\Setting;

class FilesController extends Controller
{
    private $repository;

    private $settings_repository;

    public function __construct()
    {
        if( $this->repository == null )
        {
            $this->repository = new FilesRepository();
        }

        if( $this->settings_repository == null )
        {
            $this->settings_repository = new SettingsRepository();
        }
    }

    public function download($id)
    {
        $file = File::findOrFail($id);

        $response = $this->repository->download($id);

        if( ! $response )
        {
            return response()->view('errors/exceeded');
        }

        return new RedirectResponse($response);
    }

    public function index(Request $request)
    {
        $data = $request->all();

        $limit = $this->settings_repository->by_key('files_pagination')->value ?? 20;

        $page = intval( $data['page'] ?? 1 );

        $tag =  (empty($data['tag']) || $data['tag'] == 0) ? null : intval( $data['tag'] );

        $terms = $data['s'] ?? '';

        if( $tag != null )
        {
            $files = $this->repository->all_by_tag( $page, $limit, $tag );
            return response()->json($files);
        }

        if(!empty($terms))
        {
            $files = $this->repository->all_by_search( $page, $limit, $terms );
            return response()->json($files);
        }

        $files = $this->repository->all( $page, $limit );

        return response()->json($files);
    }

    public function search(Request $request)
    {
        $data = $request->all();

        $terms = $data['s'] ?? '';

        $limit = $this->settings_repository->by_key('files_pagination')->value ?? 20;

        $page = intval( $data['page'] ?? 1 );

        if(empty($terms))
        {
            $files = $this->repository->all( $page, $limit );
            return response()->json($files);
        }

        $files = $this->repository->all_by_search($page, $limit, $terms);

        return response()->json($files);
    }

    public function store(Request $request)
    {
        $data = $request->all();

        if( $request->hasFile('cover') )
        {
            $data['cover'] = $request->file('cover');
        }

        $data['user_id'] = Auth::user()['id'];

        return response()->json($this->repository->add($data));
    }

    public function update($id, Request $request)
    {
        $data = $request->all();

        if( $request->hasFile('cover') )
        {
            $data['cover'] = $request->file('cover');
        }

        return response()->json($this->repository->update($id, $data));
    }

    public function delete(Request $request)
    {
        $data = $request->all();

        $id = intval( $data['id'] ?? 0 );

        return response()->json($this->repository->delete($id));
    }
}
