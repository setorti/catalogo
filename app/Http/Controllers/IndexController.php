<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Repository\FilesRepository;
use App\Http\Repository\SettingsRepository;

use Auth;
use App\Setting;

class IndexController extends Controller
{
    private $settings_repository;

    private $files_repository;

    public function __construct()
    {
        if( $this->files_repository == null )
        {
            $this->files_repository = new FilesRepository();
        }

        if( $this->settings_repository == null )
        {
            $this->settings_repository = new SettingsRepository();
        }
    }

    public function index()
    {
        $limit = $this->settings_repository->by_key('files_pagination')->value ?? 20;

        $number_of_pages = $this->files_repository->number_of_pages();

        return view("index/index", compact('limit', 'number_of_pages'));
    }
    
    public function institucional()
    {
        $limit = $this->settings_repository->by_key('files_pagination')->value ?? 20;

        $number_of_pages = $this->files_repository->number_of_pages();

        return view("users/institucional", compact('limit', 'number_of_pages'));
    }
    
    public function artes()
    {
        $limit = $this->settings_repository->by_key('files_pagination')->value ?? 20;

        $number_of_pages = $this->files_repository->number_of_pages();

        return view("users/artes", compact('limit', 'number_of_pages'));
    }
    
    public function banco()
    {
        $limit = $this->settings_repository->by_key('files_pagination')->value ?? 20;

        $number_of_pages = $this->files_repository->number_of_pages();

        return view("users/banco", compact('limit', 'number_of_pages'));
    }
    
    public function campanhas()
    {
        $limit = $this->settings_repository->by_key('files_pagination')->value ?? 20;

        $number_of_pages = $this->files_repository->number_of_pages();

        return view("users/campanhas", compact('limit', 'number_of_pages'));

    }

    public function about()
    {
        $about_text = Setting::where('key', '=', 'about_text')->first()->value ?? '';

        return view("index/about", compact("about_text"));
    }

    public function admin()
    {
        $limit = $this->settings_repository->by_key('files_pagination')->value ?? 20;

        return view("index/admin", compact('limit'));
    }
}
