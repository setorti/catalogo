<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Repository\TagsRepository;

class TagsController extends Controller
{
    private $repository;

    public function __construct()
    {
        if( $this->repository == null )
        {
            $this->repository = new TagsRepository();
        }
    }

    public function index()
    {
        $tags = $this->repository->all();
        return response()->json($tags);
    }

    public function update($id, Request $request)
    {
        $data = $request->all();

        return response()->json($this->repository->update($id, $data));
    }

    public function store(Request $request)
    {
        $data = $request->all();

        return response()->json($this->repository->add($data));
    }

    public function delete(Request $request)
    {
        $data = $request->all();

        $id = intval( $data['id'] ?? 0 );

        return response()->json($this->repository->delete($id));
    }
}
