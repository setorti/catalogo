<?php

namespace App\Http\Repository;

use Auth;
use Gate;
use App\Http\Services\MailService;
use App\Http\Repository\AbstractRepository;
use App\Http\Validators\UserValidator;
use App\UserConfirmationToken;
use App\RecoverHash;
use App\User;
use App\File;

use Carbon\Carbon;

class UsersRepository extends AbstractRepository
{
	private $userValidator;

	public function __construct()
	{
		$this->userValidator = new UserValidator();
	}

	public function register($data)
	{
		$model = $this->model();

		$validator = $this->userValidator->get($data, "register");

		if($validator->fails())
		{
			return ['success' => FALSE, 'errors' => $validator->errors()->all()];
		}

		$data['password'] = bcrypt( $data['password'] );

		$user = $model::create($data);

		if( $user->save() ) {
			return ['success' => TRUE, 'errors' => $validator->errors()->all(), 'user' => $user];
		}

		return ['success' => FALSE, 'errors' => $validator->errors()->all()];
	}

	public function login($credentials) {
		$model = $this->model();

		$validator = $this->userValidator->get($credentials, "login");

		$email = $credentials['email'];

		$password = $credentials['password'];

		if($validator->fails())
		{
			return ['success' => FALSE, 'errors' => $validator->errors()->all()];
		}

		if(Auth::attempt(compact('email', 'password')))
		{
			return ['success' => TRUE];
		}

		return ['success' => FALSE];
	}

	public function update($id, $data)
	{
		$model = $this->model();

		$object = $model::FindOrFail($id);

		if( empty($data['_token']) ) unset( $data['_token'] );

		if( empty($data['password']) ) unset( $data['password'] );

		else {
			$data['password'] = bcrypt( $data['password'] );
		}

		$object->fill($data);

		return $object->save();
	}

	public function delete($id)
	{
		$model = $this->model();

		$object = $model::FindOrFail($id);

		if( File::where(['user_id' => $id])->count() ) {
			return false;
		}

		UserConfirmationToken::where(['user_id' => $id])->delete();

		return $object->delete();
	}

	public function request_recover($data)
	{
		$email = $data['email'];

		$user = User::where('email', '=', $email)->first();

		if($user == null) return false;


		$expires = Carbon::now()->addDay(1);

		$hash = md5("catalogo-email-recover-shalom-$email-$expires");

		$url = \URL::to("/recover/recover/$hash");

		$recover_hash = new RecoverHash();

		$recover_hash->fill([
			'user_id' => $user->id,
			'hash' => $hash,
			'expires' => $expires,
		]);

		if( $recover_hash->save() )
		{
			$mailService = new MailService();
			$confirmationEmailInfo = $mailService->get_email_info('recover');
            $emailData = [
                'destination' => $email,
                'subject' => $confirmationEmailInfo['subject'],
                'body' => sprintf( $confirmationEmailInfo['body'], $url ),
            ];

			return $mailService->send( $emailData );
		}
	}

	public function recover_password($data)
	{
		if(!empty($data['recover_hash']) && !empty($data['password']))
		{
			$recover_hash = $data['recover_hash'];

			$recover_hash = RecoverHash::where('hash', '=', $recover_hash)->first();

			if( $recover_hash != null )
			{
				$user = $recover_hash->user;
				$user->password = bcrypt( $data['password'] );
				if($user->save())
				{
					return $recover_hash->delete();
				}
			}
		}

		return false;
	}

	public function confirm($confirmation) {
		if( $confirmation == null ) return FALSE;

		$user = $confirmation->user;

		$user->activation = -1;

		$mailService = new MailService();

		$confirmationEmailInfo = $mailService->get_email_info('new_user');

		$emailData = [
            'destination' => "criacao@comshalom.org",
            'subject' => $confirmationEmailInfo['subject'],
			'body' => $confirmationEmailInfo['body'],
		];

		$mailService->send( $emailData );

		return $user->save() && $confirmation->delete();
	}

	public function toggle_admin($id)
	{
		$model = $this->model();

		$object = $model::FindOrFail($id);

		if( Gate::denies('admin', $object) ) return false;

		$object->admin = ! $object->admin;

		return $object->save();
	}

	public function toggle_activation($id, $activation)
	{
		if( empty($activation) ) return false;

		$model = $this->model();

		$object = $model::findOrFail($id);

		if( Gate::denies('activation', $object) ) return false;

		$object->activation = $activation;

		return $object->save();
	}

	public function erase_downloads($id)
	{
		$model = $this->model();

		$object = $model::FindOrFail($id);

		if( Gate::denies('downloads', $object) ) return false;

		$object->downloads = 0;

		return $object->save();
	}

	public function model()
	{
		return 'App\User';
	}
}