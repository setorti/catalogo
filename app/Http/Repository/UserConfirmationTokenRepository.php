<?php

namespace App\Http\Repository;

use Carbon\Carbon;
use App\Http\Services\MailService;

class UserConfirmationTokenRepository {
    private $mailService;

    public function __construct() {
        if( $this->mailService == null ) $this->mailService = new MailService();
    }

    public function send($user) {
        $model = $this->model();

        $user_id = $user->id;

        $token = uniqid( md5( "userid{$user_id}" ) . time() );

        $expiration = Carbon::now()->addDay(2);

        $url = \URL::to('/users/confirm', $token);

        $confirmationToken = new $model();

        $confirmationToken->fill(compact('user_id', 'token', 'expiration'));

        if( $confirmationToken->save() ) {
            $confirmationEmailInfo = $this->mailService->get_email_info('confirmation');
            $emailData = [
                'destination' => $user->email,
                'subject' => $confirmationEmailInfo['subject'],
                'body' => sprintf( $confirmationEmailInfo['body'], $url, $url ),
            ];

            if( $this->mailService->send( $emailData ) ) {
                return true;
            }

            $confirmationToken->delete();
        }

        return false;
    }

    public function model() {
        return 'App\UserConfirmationToken';
    }
}