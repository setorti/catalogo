<?php

namespace App\Http\Repository;

use App\Http\Services\MailService;

class FormsRepository {
    public function __construct() {
        $this->mailService = new MailService();
    }

    public function send($data) {
        $confirmationEmailInfo = $this->mailService->get_email_info('about');
        $emailData = [
            'destination' => "criacao@comshalom.org",
            'subject' => $confirmationEmailInfo['subject'],
            'body' => sprintf( $confirmationEmailInfo['body'], $data['email'], $data['request'] ),
        ];

        return $this->mailService->send( $emailData );
    }
}