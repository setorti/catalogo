<?php

namespace App\Http\Repository;

interface IRepository
{
	public function add($data);

	public function get($id);

	public function all();

	public function all_with($field);

	public function update($id, $data);

	public function delete($id);

	public function model();

	public function order( $field, $direction );
}