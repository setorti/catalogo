<?php

namespace App\Http\Repository;

use Illuminate\Database\Eloquent\Model;

abstract class AbstractRepository implements IRepository
{
	public function add($data)
	{
		if(isset( $data['_token'] )) unset( $data['_token'] );

		$model = $this->model();

		$object = new $model();

		$object->fill($data);

		return $object->save();
	}

	public function get($id)
	{
		$model = $this->model();

		$object = $model::FindOrFail($id);

		return $object;
	}

	public function all()
	{
		$model = $this->model();

		return $model::all();
	}

	public function all_with($field)
	{
		$model = $this->model();

		return $model::with($field)->get();
	}

	public function update($id, $data)
	{
		if( $id === null ) return false;

		if(isset( $data['_token'] )) unset( $data['_token'] );

		unset( $data['id'] );

		$model = $this->model();

		$object = $model::FindOrFail($id);

		$object->fill($data);

		return $object->save();
	}

	public function delete($id)
	{
		$model = $this->model();

		$object = $model::FindOrFail($id);

		return $object->delete();
	}

	public function order( $field, $direction )
	{
		$model = $this->model();

		if( $field === null ) $field = 'id';

		if( $direction === null ) $direction = 'asc';

		$objects = $model::orderBy( $field, $direction )->get();

		return $objects;
	}

	public function model()
	{
		return '';
	}
}