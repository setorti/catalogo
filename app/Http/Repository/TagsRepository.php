<?php

namespace App\Http\Repository;

use App\Http\Repository\AbstractRepository;
use App\FileTag;

class TagsRepository extends AbstractRepository
{
	public function delete($id)
	{
		$model = $this->model();

		$object = $model::FindOrFail($id);

		$file_tags = FileTag::where('tag_id', '=', $id);

		if($file_tags->count() > 0) return false;

		return $object->delete();
	}

    public function model()
	{
		return 'App\Tag';
	}
}