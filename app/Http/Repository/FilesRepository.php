<?php

namespace App\Http\Repository;

use Auth;
use Carbon\Carbon;
use DB;
use Gate;
use App\FileTag;
use App\Setting;
use App\User;
use App\Http\Repository\AbstractRepository;
use Illuminate\Pagination\Paginator;

class FilesRepository extends AbstractRepository
{
	public function add($data)
	{
		if(isset($data['_token'])) unset($data['_token']);

		$model = $this->model();

		$file = new $model;

		$title = $data['title'];

		$description = $data['description'];

		$url = $data['url'];

		$user_id = $data['user_id'] ?? null;

		$tags = $data['tags'] ?? [ ];

		if($user_id != null && !empty($data['cover']))
		{
			$name = uniqid(time());

			$extension = $data['cover']->extension();

			$cover = "{$name}.{$extension}";

			if( $data['cover']->storeAs('storage/cover', $cover) ) 
			{
				$file->fill(compact('title', 'description', 'url', 'user_id', 'cover'));

				if($file->save())
				{
					$tags = explode(',', $tags);

					if(!empty($tags))
					{
						foreach( $tags as $file_tag_id )
						{
							$fileTag = new FileTag();
							$fileTag->fill([
								'file_id' => $file->id,
								'tag_id' => $file_tag_id,
							]);

							$fileTag->save();
						}
					}
					return $file;
				}

				\Storage::delete("storage/cover/{$cover}");
			}
		}

		return false;
	}

	public function update($id, $data)
	{
		$model = $this->model();

		$file = $model::FindOrFail($id);

		$title = $data['title'];

		$description = $data['description'];

		$url = $data['url'];

		$tags = $data['tags'] ?? [ ];

		$file->fill(compact('title', 'description', 'url'));

		if(!empty($data['cover']))
		{
			$name = uniqid(time());

			$extension = $data['cover']->extension();

			$full_name = "{$name}.{$extension}";

			if( $data['cover']->storeAs('storage/cover', $full_name) )
			{
				\Storage::delete("storage/cover/{$file->cover}");

				$file->fill(['cover' => $full_name]);
			}
		}

		if( $file->save() )
		{
			if(empty($tags)) return $file;

			FileTag::where('file_id', '=', $id)->delete();

			$tags = array_map('intval', explode(',', $tags));

			foreach( $tags as $tag )
			{
				$fileTag = new FileTag;

				$fileTag->fill(['file_id' => $id, 'tag_id' => $tag ]);

				if( $fileTag->save() ) continue;

				return false;
			}

			return $file;
		}

		return false;
	}

	public function download($id)
	{
		$model = $this->model();

		$object = $model::findOrFail($id);

		$user = User::findOrFail( Auth::user()['id'] );

		if( Gate::denies('download', $object) ) return false;

		$user->downloads++;
        $user->total_downloads++;
		$object->downloads++;

		if($user->download_expire == null)
        {
            $user->download_expire = Carbon::now()->addDay(1);
		}

		if($user->save() && $object->save())
		{
			return $object->url;
		}

		return false;
	}

	public function delete($id)
	{
		$model = $this->model();

		$object = $model::FindOrFail($id);

		foreach( $object->file_tags as $file_tag )
		{
			$file_tag->delete();
		}

		\Storage::delete("storage/cover/{$object->cover}");

		return $object->delete();
	}

	private function attach_file_tags($files)
	{
		foreach( $files as &$file )
		{
			foreach( $file->file_tags as &$file_tag )
			{
				$file_tag = $file_tag->tag;
			}
		}
	}

	public function all($page = 1, $limit = 20)
	{
		$model = $this->model();

		$files = $model::with('file_tags')->orderBy('created_at', 'DESC')->paginate( $limit, ['*'], 'page', $page );

		foreach( $files as &$file )
		{
			foreach( $file->file_tags as &$file_tag )
			{
				$file_tag = $file_tag->tag;
			}
		}

		return $files;
	}

	public function all_by_tag($page = 1, $limit = 20, $tag = null)
	{
		$model = $this->model();

		if( $tag == null ) return;

		$files = $model::with('file_tags')->orderBy('created_at', 'DESC')->whereHas('file_tags', function($q) use($tag) {

			$q->where("tag_id", "=", $tag);

		})->paginate($limit, ['*'], 'page', $page);

		foreach( $files as &$file )
		{
			foreach( $file->file_tags as &$file_tag )
			{
				$file_tag = $file_tag->tag;
			}
		}

		return $files;
	}

	public function all_by_search($page = 1, $limit = 20, $terms = null)
	{
		$model = $this->model();

		if( $terms == null ) return;

		$ids = [ ];

		$terms = array_map('strtolower', explode(' ', $terms));

		$file_tags = FileTag::orderBy('id', 'ASC')->get();

		foreach( $file_tags as $ft )
		{
			if( in_array( $ft->file->id, $ids ) ) continue; // Evitar repetição de arquivos.

			$title = $ft->file->title;

			$description = $ft->file->description;

			$tag = $ft->tag->name;

			$acceptable = TRUE;

			foreach( $terms as $term )
			{
				if( stripos( $title, $term ) === FALSE && stripos( $description, $term ) === FALSE && stripos(  $tag, $term ) === FALSE )
				{
					$acceptable = FALSE;
					break;
				}
			}

			if( $acceptable )
			{
				array_push( $ids, $ft->file->id );
			}
		}

		/*foreach( $terms as $term )
		{
			foreach( $file_tags as $ft )
			{
				if( in_array( $ft->file->id, $ids ) ) continue; // Evitar repetição de arquivos.

				$title = $ft->file->title;

				$description = $ft->file->description;

				$tag = $ft->tag->name;

				// Se encontrar ocorrências no Title, Description ou Tag name.
				if( stripos( $title, $term ) !== FALSE || stripos( $description, $term ) !== FALSE || stripos(  $tag, $term ) !== FALSE )
				{
					array_push( $ids, $ft->file->id );
				}
			}
		}*/

		$files = $model::with('file_tags')->orderBy('created_at', 'DESC')->whereHas('file_tags', function($q) use($ids) {

			$q->whereIn("file_id", $ids);

		})->paginate($limit, ['*'], 'page', $page);

		$this->attach_file_tags($files);

		return $files;
	}

	public function number_of_pages()
	{
		$pages = DB::select(DB::raw("select CEILING(COUNT(files.id) / settings.value) as pages from files LEFT JOIN settings ON `key` = 'files_pagination'"))[0]->pages ?? 0;

		return $pages;
	}

	public function model()
	{
		return 'App\File';
	}
}