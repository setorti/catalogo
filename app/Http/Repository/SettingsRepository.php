<?php

namespace App\Http\Repository;

use App\Http\Repository\AbstractRepository;
use App\Setting;

class SettingsRepository extends AbstractRepository
{
	public function all()
	{
		return parent::all()->keyBy('key');
	}

	public function update_all($data)
	{
		$model = $this->model();

		foreach( $data as $setting )
		{
			unset( $setting['created_at'] );
			unset( $setting['updated_at'] );

			$key = $setting['key'];

			$object = $model::where('key', '=', $key)->firstOrFail();

			$object->fill( $setting );

			if( ! $object->save() ) {
				return false;
			}
		}

		return true;
	}

	public function by_key($key)
	{
		$setting = Setting::where('key', '=', $key)->first();
		return $setting;
	}

	public function model()
	{
		return 'App\Setting';
	}
}