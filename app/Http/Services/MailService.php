<?php

namespace App\Http\Services;

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class MailService
{
    private $email_info = [
        'about' => [
            'subject' => 'Catálogo de Comunicação Shalom - Formulário Sobre o Catálogo',
            'body' => "<p>O usuário de email <b>%s</b> enviou um questionamento através do formulário da página 'Sobre o Catálogo'.</p> <hr> <p>%s</p>",
        ],
        'confirmation' => [
            'subject' => 'Catálogo de Comunicação Shalom - Link para Ativação de seu Acesso',
            'body'    => "<p>Bem-vindo ao Catálogo de Comunicação Shalom! Clique no link abaixo para ativar sua conta. Em seguida, o administrador do sistema irá aprovar seu cadastro.</p><p><a href='%s'>%s</a></p>",
        ],
        'new_user' => [
            'subject' => 'Catálogo de Comunicação Shalom - Novo Usuário Cadastrado',
            'body' => "<p>Há um novo usuário cadastrado no catálogo aguardando por sua aprovação! Verifiquei o painel de administrador para maiores detalhes.</p>",
        ],
        'recover' => [
            'subject' => 'Catálogo de Comunicação Shalom - Recuperação de Senha',
            'body' => "<h3>Recuperação de senha - Catálogo Shalom</h3><p>Se você não solicitou recuperação de senha, <b>ignore este email.</b></p><br><br><p>Para recuperar sua senha, acesse o link a seguir: <a href='%s'>ALTERAR SENHA.</a>"
        ],
    ];

    private $default_credentials = [
        'username' => 'criacao@comshalom.org',
        'password' => 'P@rresi401',
        'smtp' => 'smtp.gmail.com',
        'security' => 'tls',
        'port' => 587,
    ];

    private $smtp_options = [
        'ssl' => [
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
        ]
    ];

    public function send($data)
    {
        if(empty($data['subject']) || empty($data['body']) || empty($data['destination'])) return false;

        try
        {
            $mail = new PHPMailer(true);
            $mail->SMTPOptions = $this->smtp_options;
            $mail->isSMTP();
			$mail->SMTPAuth = true;
			$mail->Host = $this->default_credentials['smtp'];
			$mail->Port = $this->default_credentials['port'];
			$mail->Username = $this->default_credentials['username'];
			$mail->Password = $this->default_credentials['password'];
			$mail->SMTPSecure = $this->default_credentials['security'];
			$mail->isHTML(true);
			$mail->CharSet = 'UTF-8';
			$mail->Subject = $data['subject'];
            $mail->Body    = $data['body'];

            $mail->setFrom($this->default_credentials['username'], 'Catálogo de Comunicação Shalom');
			$mail->addAddress($data['destination']);

			return $mail->send();
        }
        catch(\Exception $e)
		{
            echo $e->getMessage();
            return false;
		}
    }

    public function get_email_info($key)
    {
        return $this->email_info[ $key ] ?? [ ];
    }
}