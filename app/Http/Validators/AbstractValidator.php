<?php

namespace App\Http\Validators;

abstract class AbstractValidator
{
    protected $validators = [];

    public function get($data, $key)
    {
        $validator = $this->validators[ $key ];

        $rules = $validator['rules'] ?? [ ];

        $messages = $validator['messages'] ?? [ ];

        return \Validator::make( $data, $rules, $messages );
    }

    public function model()
    {
        return 'App\User';
    }
}