<?php

namespace App\Http\Validators;

class UserValidator extends AbstractValidator
{
    public function __construct()
    {
        $this->validators['login'] = [
            'rules' => [
                'email' => 'required|email|max:50',
                'password' => 'required|min:6|max:20'
            ],
            'messages' => [],
        ];

        $this->validators['register'] = [
            'rules' => [
                'name' => 'required|min:6|max:50',
                'email' => 'required|email|max:50|unique:users,email|regex:/[a-zA-Z]+[a-zA-Z0-9]*\@comshalom\.org/',
                'password' => 'required|min:6|max:20',
            ],
            'messages' => [
                'name.min' => 'O nome deve conter pelo menos :min caracteres',
                'name.max' => 'O nome deve conter até :max caracteres',
                'email.unique' => 'O email solicitado já foi cadastrado.',
                'email.regex' => 'O email cadastrado deve ser do domínio comshalom.org.'
            ],
        ];
    }

    public function model()
    {
        return 'App\User';
    }
}