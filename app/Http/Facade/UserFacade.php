<?php

namespace App\Http\Facade;

use App\Http\Repository\UsersRepository;
use App\Http\Repository\UserConfirmationTokenRepository;

class UserFacade {
    private $userRepository;
    private $userConfirmationTokenRepository;

    public function __construct() {
        $this->userRepository = new UsersRepository();
        $this->userConfirmationTokenRepository = new UserConfirmationTokenRepository();
    }

    public function register($data) {
        $result = $this->userRepository->register($data);

        if( $result['success'] === TRUE && !empty($result['user']) ) {
            $user = $result['user'];
            return $this->userConfirmationTokenRepository->send($user);
        }

        return $result;
    }
}