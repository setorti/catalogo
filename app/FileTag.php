<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FileTag extends Model
{
	protected $fillable = [
		'file_id',
		'tag_id',
	];

    public function tag()
    {
    	return $this->belongsTo('App\Tag');
    }

    public function file()
    {
    	return $this->belongsTo('App\File');
    }
}
