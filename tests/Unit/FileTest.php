<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use DB;

use App\Setting;
use App\File;

class FileTest extends TestCase
{
    public function testPagination()
    {
        $pages = DB::select(DB::raw("select CEILING(COUNT(files.id) / settings.value) as pages from files LEFT JOIN settings ON `key` = 'files_pagination'"))[0]->pages;

        $files_count = DB::select(DB::raw("SELECT COUNT(*) as count FROM files"))[0]->count;

        $pagination = DB::select(DB::raw("SELECT value FROM settings WHERE `key` = 'files_pagination'"))[0]->value;

        $this->assertEquals($pages, ceil($files_count/$pagination));
    }
}
