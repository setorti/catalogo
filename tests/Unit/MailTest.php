<?php

namespace Tests\Unit;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

use App\Http\Services\MailService;

class MailTest extends TestCase
{
    public function testMail()
    {
        $data = [
            'destination' => 'informatica@comshalom.org',
            'subject' => 'Test Mail - PHPUnit - Catalogo Shalom',
            'body' => '<b>Email de Testes com PHPUnit.</b>',
        ];

        $service = new MailService();
        $result = $service->send($data);
        $this->assertTrue($result);
    }
}
