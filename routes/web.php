<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/about', ['uses' => 'IndexController@about']);
Route::get('/logout', ['uses' => 'UsersController@logout']);

// Users.
Route::get('/users/confirm/{token}', ['uses' => 'UsersController@confirm']);

// RecoverHash.
Route::get('/recover/recover/{hash}', ['uses' => 'RecoverHashController@recover']);

Route::group(['middleware' => ['activator']], function() {
    Route::get('/', ['uses' => 'IndexController@index']);
    
    // Users
    Route::get('/login', ['as' => 'login', 'uses' => 'UsersController@login']);
    Route::get('/register', ['uses' => 'UsersController@register']);
    Route::post('/auth/login', ['uses' => 'AuthController@login']);
    Route::post('/auth/register', ['uses' => 'AuthController@register']);
    
    Route::group(['middleware' => ['auth']], function() {
        
        Route::get('/institucional', ['uses' => 'IndexController@institucional']);
        Route::get('/artes', ['uses' => 'IndexController@artes']);
        Route::get('/banco', ['uses' => 'IndexController@banco']);

        Route::get('/campanhas', ['uses' => 'IndexController@campanhas']);

        Route::get('/welcome', ['uses' => 'UsersController@welcome']);

        Route::get('/admin', ['uses' => 'IndexController@admin', 'admin' => TRUE]);
        Route::get('/users/edit/{id}', ['uses' => 'UsersController@edit']);
        Route::post('/users/update/{id}', ['uses' => 'UsersController@update']);

        // Files
        Route::get('/files/download/{id}', ['uses' => 'FilesController@download']);
    });

    // API ROUTES.
    Route::prefix('api')->group(function() {
        Route::get('/tags', ['uses' => 'TagsController@index']);
        Route::get('/files', ['uses' => 'FilesController@index']);
        Route::get('/search', ['uses' => 'FilesController@search']);

        // RecoverHash.
        Route::post('/recover/request', ['uses' => 'RecoverHashController@request']);
        Route::post('/recover/password', ['uses' => 'RecoverHashController@password']);

        // Users.
        Route::get('/users', ['uses' => 'UsersController@index']);

        // Settings.
        Route::get('/settings', ['uses' => 'SettingsController@index']);
        Route::get('/settings/get/', ['uses' => 'SettingsController@get']);

        Route::group(['middleware' => ['auth']], function() {
            Route::post('/forms/about', ['uses' => 'FormsController@about']);

            // Tags.
            Route::post('/tags/store', ['uses' => 'TagsController@store']);
            Route::post('/tags/update/{id}', ['uses' => 'TagsController@update']);
            Route::post('/tags/delete', ['uses' => 'TagsController@delete']);

            // Files.
            Route::post('/files/store', ['uses' => 'FilesController@store']);
            Route::post('/files/update/{id}', ['uses' => 'FilesController@update']);
            Route::post('/files/delete', ['uses' => 'FilesController@delete']);

            // Settings.
            Route::post('/settings/update-all/', ['uses' => 'SettingsController@update_all']);

            // Users.
            Route::get('/users/delete/{id}', ['uses' => 'UsersController@delete']);
            Route::get('/users/toggle/admin/{id}', ['uses' => 'UsersController@toggle_admin']);
            Route::post('/users/toggle/activation/{id}', ['uses' => 'UsersController@toggle_activation']);
            Route::get('/users/erase/downloads/{id}', ['uses' => 'UsersController@erase_downloads']);
        });
    });
});